val authors = setOf("Shakespeare", "Hemingway", "Twain")
val writers = setOf("Twain", "Shakespeare", "Hemingway")

fun main() {
    println(authors == writers)
    println(authors === writers)
}
