fun main() {
    println("Hello, World!")

//    #2
//    printMessage("Hello")
//    printMessageWithPrefix("Hello", "Log")
//    printMessageWithPrefix("Hello")
//    printMessageWithPrefix(prefix = "Log", message = "Hello")
//    println(sum(3, 4))

//    #3
//    infix fun Int.times(str: String) = str.repeat(this)
//    println(2 times "Bye ")
//
//    val pair = "Ferrari" to "katrina"
//    println(pair)
//
//    infix fun String.onto(other: String) = Pair(this, other)
//    val myPair = "McLaren" onto "Lucas"
//    println(myPair)
//
//    val sophia = Person("Sophia")
//    val claudia = Person("Claudia")
//    sophia likes claudia

//    #4
//    println(2 * "Bye ")
//    println(str[0..15])

//    #5

}

// #Functions
// #2
fun printMessage(message: String): Unit {
    println(message)
}
fun printMessageWithPrefix(message: String, prefix: String = "Into"){
    println("[$prefix] $message")
}
fun sum(x: Int, y: Int): Int {
    return x + y
}
fun multiply(x: Int, y: Int) = x * y

// #3
class Person(val name: String) {
    val likedPeople = mutableListOf<Person>()
    infix fun likes(other: Person) { likedPeople.add(other) }
}
// #4
operator fun Int.times(str: String) = str.repeat(this)

operator fun String.get(range: IntRange) = substring(range)
val str = "Always forgive your enemies; nothing annoys them so much."

//#5


