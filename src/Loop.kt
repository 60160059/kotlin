//fun eatACake() = println("Eat a Cake")
//fun bakeACake() = println("Bake a Cake")
//
//fun main(args: Array<String>) {
//    var cakesEaten = 0
//    var cakesBaked = 0
//
//    while (cakesEaten < 5) {
//        eatACake()
//        cakesEaten ++
//    }
//
//    do {
//        bakeACake()
//        cakesBaked++
//    } while (cakesBaked < cakesEaten)
//
//}

//val cakes = listOf("carrot", "cheese", "chocolate")
//
//fun main(args: Array<String>) {
//    for (cake in cakes) {                               // 1
//        println("Yummy, it's a $cake cake!")
//    }
//}

class Animal(val name: String)

class Zoo(val animals: List<Animal>) {

    operator fun iterator(): Iterator<Animal> {
        return animals.iterator()
    }
}

fun main() {

    val zoo = Zoo(listOf(Animal("zebra"), Animal("lion")))

    for (animal in zoo) {
        println("Watch out, it's a ${animal.name}")
    }

}
